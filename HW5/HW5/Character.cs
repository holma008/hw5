﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HW5
{
    public class Character
    {
        public ImageSource Picture
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string FullName
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public string Race
        {
            get;
            set;
        }
        public string Class
        {
            get;
            set;
        }
    }
}
