﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW5
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Character> collection;
        public MainPage()
        {
            InitializeComponent();
            collection = new ObservableCollection<Character>();
            PopulateList();
        }
        private void HandleMoreClicked(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var character = (Character)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreCharacterInfo(character));
        }
        private void HandleDeleteClicked(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var character = (Character)menuItem.CommandParameter;
            collection.Remove(character);
        }
        private void PopulateList()
        {
            var Character1 = new Character
            {
                Picture = ImageSource.FromFile("Arthas.png"),
                Name = "Arthas",
                FullName = "Full Name: Arthas Menethil",
                Title = "Title: Lich King",
                Race = "Race: Human",
                Class = "Class: Death Knight",
            };
            collection.Add(Character1);
            var Character2 = new Character
            {
                Picture = ImageSource.FromFile("Jaina.png"),
                Name = "Jaina",
                FullName = "Full Name: Jaina Proudmoore",
                Title = "Title: Lord Admiral of Kul'Tiras",
                Race = "Race: Human",
                Class = "Class: Mage",
            };
            collection.Add(Character2);
            var Character3 = new Character
            {
                Picture = ImageSource.FromFile("Illidan.png"),
                Name = "Illidan",
                FullName = "Full Name: Illidan Stormrage",
                Title = "Title: The Betrayer",
                Race = "Race: Night Elf",
                Class = "Class: Demon Hunter",
            };
            collection.Add(Character3);
            var Character4 = new Character
            {
                Picture = ImageSource.FromFile("Thrall.png"),
                Name = "Thrall",
                FullName = "Birth Name: Go'el",
                Title = "Title: Warchief of the Horde",
                Race = "Race: Orc",
                Class = "Class: Shamman",
            };
            collection.Add(Character4);
            var Character5 = new Character
            {
                Picture = ImageSource.FromFile("Kelthuzad.png"),
                Name = "Kel'Thuzad",
                FullName = "Full Name: Kel'Thuzad",
                Title = "Title: Founder of the Cult of the Damned",
                Race = "Race: Undead",
                Class = "Class: Archlich",
            };
            collection.Add(Character5);

            WC_Characters.ItemsSource = collection;
        }
    }
}
