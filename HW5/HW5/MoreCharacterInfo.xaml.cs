﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW5
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreCharacterInfo : ContentPage
	{
		public MoreCharacterInfo ()
		{
			InitializeComponent ();
		}

        public MoreCharacterInfo(Character character)
        {
            InitializeComponent();

            BindingContext = character;
        }
	}
}